# About

The [Digital Skills Framework](https://www.gov.uk/government/publications/essential-digital-skills-framework/essential-digital-skills-framework), is designed to set out 5 key areas that people need to be able to use digital technology both at home and at work.

The 5 key areas are:

1. [Communicating](./communicating.md)
2. [Handling information and content](./handleinfo.md)
3. [Transacting](./transacting.md)
4. [Problem solving](./problemsolving.md)
5. [Being safe and legal online](./safeandlegal.md)

As well as this, there are also [Foundation](./foundation.md) skills, that
cover the use of devices (turn on / off, login, connect to networks) so more
fundamental use of hardware and operating systems.

The aim, for this resource, is to cover each of these key areas using Free, Libre, and Open Source software alternatives. This project was started by [Paul Sutton](https://personaljournal.ca/paulsutton/)
