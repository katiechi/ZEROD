# Wellbeing

Looking after your mental health


* [digital Wellbeing](https://enhance.etfoundation.co.uk/modules/2029/digital-wellbeing )

	* Protecting privacy
	* Protecting data (risks)
	* Being safe and responsible online
