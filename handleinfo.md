# Handling information and content

The skills required to find, manage and store digital information and content securely.

## Skills for life

I can:

* understand that not all online information and content that I see is reliable
* evaluate what information or content may, or may not, be reliable
* use search engines to find information and make use of search terms to generate better results
* use bookmarks to save and retrieve information on my web browser
* access information and content from different devices
* understand that the cloud is a way that I can store information and content in a remote location.
* organise my information and content using files and folders on my device or on the cloud
* use the internet to legally access content for entertainment including films, music, games and books

## Skills for life examples

I can:

* understand that not all entries in online encyclopaedias, such as Wikipedia, are true or reliable
* search for news using a browser such as Chrome, Internet Explorer or Safari
* use a cloud storage account for a music or photo collection (from legal sources such as Apple iCloud, Instagram) and access the collections from different devices, such as a laptop or a smartphone
* stream music from legal sites such as Spotify or Apple Music, or watch streamed movies from legal sources such as Netflix or Amazon Prime

The information presented above is done so under the conditions set out on the [license](./README.md) page

<hr>


## How we can do this with Free, Libre, and Open Source software and privacy friendly alternatives

1. [Wikipedia](Wikipedia.org)
   * Wikipedia is not considered an academic source of information.
   * It is useful as a reference or for finding links to other information (via the references / citations).
   * There are lots of resource such as [IOPscience](https://iopscience.iop.org/) that are more useful.
   * Good research requires time, effort and work to get it right.
   * Websites such as medical / science journals (BMJ, IOPscience, arXiv) are usually far better at providing accurate information, you can ask on forums such as scienceforums for help on these sources if you have *read* but don't understand something.
   * Remember that wikipedia is constantly changing, so you could cite a page / resource, then when someone goes to check that, it may have changed.

2. Search engines and web browsers
   * [DuckDuckGo](https://duckduckgo.com/)
   * [Searx](https://searx.me/) - metasearch engine
     * [List of searx instances](https://searx.space/)
     * [Snopyta Searx](https://search.snopyta.org/)
     * [Disroot SearX](https://search.disroot.org/)
   * [Firefox Browser](https://www.mozilla.org/en-GB/firefox/new/)[^firefox]
   * [GNU IceCat](https://www.gnu.org/software/gnuzilla/)

3. Miscellaneous services
   * [Nextcloud](https://nextcloud.com/) - online "cloud" storage
   * [Disroot](https://www.disroot.org/) - various services
     * [List of Disroot services](https://disroot.org/en/#services)
   * [Cryptpad](https://cryptpad.fr) - encrypted collaborative documents
     * [Disroot Cryptpad](https://cryptpad.disroot.org/)

4. Alternative media streaming services
   * [Creative Commons](https://creativecommons.org/)
   * [Funkwhale](https://funkwhale.audio/) - audio
   * [Peertube](https://joinpeertube.org/) - video
   * [Invidious](https://invidious.snopyta.org/) - Free Software online YouTube viewer
   * [youtube-dl](https://yt-dl.org/) - Free Software YouTube downloader

Link to DRM and digital restrictions management, (both) explain how drm also gives you a license to use content, rather than the ability to use, the fair use for copying for personal use (e.g cd to mp3/ogg) is removed. Users are treated as IF they are going to breach copyright.

[^firefox]: Firefox contains some obfuscated and encrypted code (like [this](https://dxr.mozilla.org/comm-central/source/comm/mail/components/cloudfile/nsBox.js#807) and [this](https://dxr.mozilla.org/comm-central/source/comm/calendar/providers/caldav/calDavCalendar.js#2978))
