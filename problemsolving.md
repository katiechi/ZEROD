# Problem solving

The skills required to find solutions to problems using digital tools and online services.

## Skills for life

I can:

* Use the internet to find information that helps me solve problems
* Use the internet to find sources of help for a range of activities
* Use chat facilities (where available) on websites to help me solve problems
* Use online tutorials, FAQs and advice forums to solve problems and improve my skills in using devices, software and applications

## Skills for life examples

I can:

* use the internet to find specific information related to Life tasks that need to be carried out, for example finding a recipe, or finding information that helps plan travel
* use the help, FAQ section or chat facility of a manufacturer’s website or other related content to work out how to fix an issue with a device
* find out how to do something by using a tutorial video such as those found on YouTube

The information presented above is done so under the conditions set out on the [Source](./README.md) page

<hr>

How we can do this with; Free Software, Open source software and privacy friendly websites.

The ideas below cover all 3 of the points above, there is no one source of information, you should use more than one where possible.

Use Privacy friendly search engines such as:
* [Duckduckgo](https://duckduckgo.com/)
* [Searx](https://searx.me/) - metasearch engine
  * [List of searx instances](https://searx.space/)
  * [Snopyta Searx](https://search.snopyta.org/)
  * [Disroot SearX](https://search.disroot.org/)

[Open Street Map](https://www.openstreetmap.org/) (alternative to Google and other non free mapping tools)

For GNU/Linux there are:

* [Man Pages](https://linux.die.net/man/) which provide information on commands, or just use the `man` command

E-mail discussion lists are a good source of help, you should ask *after* you have tried to research the problem so you can explain steps taken.

Documentation is usually a good source of help, for example the [Debian support](https://www.debian.org/support) community or the [Libreoffice Documentation](https://documentation.libreoffice.org/en/english-documentation/) is developed by the community.

Internet relay chat or IRC is also a source of help, you need to ask and wait for a reponse. More information on what IRC is can be found [here](https://www.irchelp.org/)

Again you are usually expected to search for the answer, the hardest part of this is knowing what to search for. So it is fine to have  go, and then ask for help on how best to refine your search.

You may find that some communities are rather strict on how you ask for help or respond for example it is usally the case you don't top post, but reply under the part of the e-mail you are responding to. This is set out in [RFC1855](https://tools.ietf.org/html/rfc1855) Netiquette Guidelines. If you join a community, lurk for a while and get to know how that community works / operates.

Rather than looking purely on YouTube for tutorials there may be some on [Peertube](https://joinpeertube.org/) or on the communities that are part of the decentralised social media networks such as [Mastodon](https://joinmastodon.org/). If it is only on YouTube, you can use [Invidious](https://invidious.snopyta.org/) or [youtube-dl](https://yt-dl.org/).
